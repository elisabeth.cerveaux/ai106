package fr.afcepf.ai106.artistes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import fr.afcepf.ai106.musique.Instrument;

@Component
public class Musicien implements Performeur
{
	private String morceau;
	
	// injection automatique par autowiring :
	@Autowired
	private Instrument instrumentActuel;
	// je pr�cise le nom du composant � injecter (guit)
	@Autowired
	@Qualifier(value = "guit")
	private Instrument instrumentSecondaire;
	
	public Musicien()
	{
		System.out.println("je cr�e un musicien");
	}
	
	public String getMorceau() {
		return morceau;
	}



	public void setMorceau(String morceau) {
		this.morceau = morceau;
	}



	public Instrument getInstrumentActuel() {
		return instrumentActuel;
	}



	public void setInstrumentActuel(Instrument instrumentActuel) {
		this.instrumentActuel = instrumentActuel;
	}



	public void perform() 
	{
		System.out.println("Je joue " + morceau);
		instrumentActuel.jouer();
		instrumentSecondaire.jouer();
	}

	public Instrument getInstrumentSecondaire() {
		return instrumentSecondaire;
	}

	public void setInstrumentSecondaire(Instrument instrumentSecondaire) {
		this.instrumentSecondaire = instrumentSecondaire;
	}
	
	
}
