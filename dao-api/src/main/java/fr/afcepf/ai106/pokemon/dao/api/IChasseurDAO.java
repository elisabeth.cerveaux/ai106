package fr.afcepf.ai106.pokemon.dao.api;

import java.util.List;

import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

public interface IChasseurDAO 
{
	public List<ChasseurDTO> getAll();
	
	public ChasseurDTO getById(int id);
}
